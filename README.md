# Decorator

Liviu-Octavian STAMATOIU's instructions for installing and running his bachelor's project.

"Decorator" is the informal name for my bachelor's project. Henceforth, any reference to
"Decorator" corresponds to my bachelor's project titled "Augmented Reality Application for Interior Decor with Colour Passthrough".

## Needed to run the app

This application is built with Unity for the Meta Quest 3. As such, to build and run, you need:

A Meta Quest 3 headset, with an Oculus account configured to have developer mode unlocked.

A USB Type-C cable to connect from a computer to the headset.

Unity Hub with editor version 2022.3.11f1 installed.


## Installation

Have your Meta Quest 3 headset booted up, logged in on an Oculus account with with developer mode activated.

Connect the headset to the computer via USB Type-C and ensure you allow the computer to access files on the headset. 

Clone this Github project and open it with Unity Hub using the "Add Project From Disk" option.

Allow the project to download relevant packages as it opens for the first time from the Unity Hub (this could take some time).

Go to Edit > Project Settings > Meta XR and click "Apply All" in the "Recommended Items" section to auto-fix the configuration.

Within the main view in Unity, navigate to the Project tab in the lower left part of the window and click on the Scenes folder.

From the scenes folder, double click on "FinalSceneBeforeFixes" and make sure it's loaded in Scene view.

With the scene loaded, navigate to Build Settings to the Android tab, and click "Build And Run" (this button will be greyed out if the IDE doesn't recognize your headset).

Once the build is done (could take upwards of 10 minutes at first build), put on your headset, close and re-open the Decorator app.

All done! The app should run on the headset, prompting the user to make a scan of their room with the native OS functionality. Afterwards it should run as usual on every future opening of the program as long as the user is in the same, scanned room.

## Authors and acknowledgment

This application is entirely original except for the following packages:

* Chris Nolet's Quick Outline package for Unity
* Unity Technologies' Particle Pack package for Unity
* Meta XR All-In-One SDK for Unity
* 3D models of IKEA furniture from polantis.com
* The Clap Detection script as described by Reddit user /u/giinsen (see references in the documentation)