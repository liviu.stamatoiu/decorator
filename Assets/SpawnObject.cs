using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    // Start is called before the first frame update

    public SelectionManager selectionManager;

  public void SpawnArgument(GameObject toSpawn)
    {
        GameObject spawnedObject;
        spawnedObject = Instantiate(toSpawn, transform.position, Quaternion.identity);
        selectionManager.SelectObject(spawnedObject);
        //if (spawnedObject.GetComponent<SnapToFloor>())
        //    spawnedObject.GetComponent<SnapToFloor>().SnapToFloorPosition();



    }
}
