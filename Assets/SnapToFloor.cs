using UnityEngine;

public class SnapToFloor : MonoBehaviour
{

    private Transform toSnap;
    private GameObject toSnapGameObject;
    private Renderer toSnapRenderer;

    private void Start()
    {
        toSnap = GetComponent<Transform>();
        //toSnapGameObject = toSnap.gameObject;
        //toSnapRenderer = GetComponent<Renderer>();

        SnapToFloorPosition();
    }

    public void SnapToFloorPosition()
    {
        // Get the collider attached to the object
        Collider collider = GetComponent<Collider>();
        if (collider == null)
        {
            Debug.LogError("Collider not found on object: " + gameObject.name);
            return;
        }

        // Raycast downward to find the floor
        // Use offset to figure out the distance from the object's transform to its collider's bottom, to align with the floor etc
        RaycastHit hit;
        float offset = toSnap.position.y - (collider.bounds.center.y - collider.bounds.extents.y);
        if (Physics.Raycast(toSnap.position/*toSnapRenderer.bounds.center*/, Vector3.down, out hit))
        {
            // Calculate the distance from the object's center to the bottom of its collider
            // Snap the object's position to the hit point on the floor, taking into account the distance to the bottom
            Vector3 newPosition = hit.point - offset * Vector3.down;
            toSnap.position = newPosition;

        }
    }
}
