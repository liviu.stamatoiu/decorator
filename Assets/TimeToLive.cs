using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeToLive : MonoBehaviour
{
    // Start is called before the first frame update

    public int destructionTime;
    void Awake()
    {
        Destroy(gameObject, destructionTime);
    }

}
