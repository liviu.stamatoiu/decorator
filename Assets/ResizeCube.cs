using UnityEngine;

[ExecuteInEditMode]
public class ResizeCube : MonoBehaviour
{
    public GameObject furnitureModel;

    void Update()
    {
        if (furnitureModel == null)
        {
            Debug.LogError("Furniture model is not assigned!");
            return;
        }

        ResizeToFit();
    }

    void ResizeToFit()
    {
        // Get all MeshRenderers in the furniture model
        MeshRenderer[] meshRenderers = furnitureModel.GetComponentsInChildren<MeshRenderer>();
        if (meshRenderers.Length == 0)
        {
            Debug.LogWarning("No MeshRenderers found in the furniture model.");
            return;
        }

        // Initialize the bounds with the first renderer's bounds
        Bounds combinedBounds = meshRenderers[0].bounds;

        // Combine the bounds of all MeshRenderers
        foreach (MeshRenderer renderer in meshRenderers)
        {
            combinedBounds.Encapsulate(renderer.bounds);
        }

        // Set the position and size of the cube to fit the combined bounds
        transform.position = combinedBounds.center;
        transform.localScale = combinedBounds.size;
    }
}
