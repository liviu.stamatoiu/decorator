using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleActiveMultiple : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] gameObjects;
    
    public void toggleObjectsState()
    {
        foreach (GameObject go in gameObjects)
        {
            go.SetActive(!go.activeSelf);
        }
    }
}
