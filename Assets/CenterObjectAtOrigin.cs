using UnityEngine;

[ExecuteInEditMode]
public class CenterObjectAtOrigin : MonoBehaviour
{
    void Update()
    {
        CenterAtOrigin();
    }

    void CenterAtOrigin()
    {
        // Get all MeshRenderers in the object and its children
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        if (meshRenderers.Length == 0)
        {
            Debug.LogWarning("No MeshRenderers found.");
            return;
        }

        // Calculate the combined bounds of all MeshRenderers
        Bounds combinedBounds = meshRenderers[0].bounds;
        foreach (MeshRenderer renderer in meshRenderers)
        {
            combinedBounds.Encapsulate(renderer.bounds);
        }

        // Calculate the offset to move the center to the origin
        Vector3 offset = combinedBounds.center;

        // Move the transform to the origin
        transform.position -= offset;

    }
}
