using Oculus.Interaction.Samples;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;

public class MoveWithBody : MonoBehaviour
{

    public GameObject headset;
    public GameObject toolbelt;

    public float downOffset;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPos = new Vector3(headset.transform.position.x, headset.transform.position.y - downOffset, headset.transform.position.z);
        Quaternion newRot = new Quaternion(toolbelt.transform.rotation.x, headset.transform.rotation.y, toolbelt.transform.rotation.z, toolbelt.transform.rotation.w);

        toolbelt.transform.SetPositionAndRotation(newPos, newRot);
    }
}
