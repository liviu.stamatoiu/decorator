using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetFirstColliders : MonoBehaviour
{
    public BoxCollider[] roomWalls;

    // Start is called before the first frame update
    void Start()
    {
        Invoke("getWalls", 3);
    }

    void getWalls()
    {
        roomWalls = FindObjectsOfType<BoxCollider>();
        roomWalls = (BoxCollider[]) roomWalls.Clone();
    }
}
