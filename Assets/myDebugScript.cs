using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class myDebugScript : MonoBehaviour
{


    public TextMeshProUGUI debugText;

    // Start is called before the first frame update
    void Start()
    {
        debugText.text = "Waiting for input from lefthand";
    }

    // Update is called once per frame
    void Update()
    {


        if (OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.Hands))
            debugText.text = "Something happened!";

    }
}
