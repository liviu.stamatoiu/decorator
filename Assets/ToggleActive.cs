using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleActive : MonoBehaviour
{
    public GameObject activeGameObject;

    public void toggleObjectState()
    {
        activeGameObject.SetActive(!activeGameObject.activeSelf);
    }
}
