using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR;

public class GetAnchorLabels : MonoBehaviour
{
    [SerializeField] private LineRenderer lineRenderer;
    public OVRHand hand;
    public TextMeshProUGUI debugText;
    private OVRSceneManager sceneManager;
    private OVRSceneRoom sceneRoom;
    private OVRScenePlane[] roomWalls;


    private void SceneLoaded()
    {
        sceneRoom = FindObjectOfType<OVRSceneRoom>();
        roomWalls = sceneRoom.Walls;
    }
    private void Awake()
    {
        sceneManager = FindObjectOfType<OVRSceneManager>();
        sceneManager.SceneModelLoadedSuccessfully += SceneLoaded;
        debugText.text = "";

        foreach (var wall in roomWalls)
        {
            debugText.text += wall.ToString() + " ";
        }

    }

    void Update()
    {
        Vector3 handPosition = hand.PointerPose.position;
        Quaternion handRotation = hand.PointerPose.rotation;
        Vector3 rayDirection = handRotation * Vector3.forward;

        if (Physics.Raycast(handPosition, rayDirection, out RaycastHit hit))
        {
            lineRenderer.SetPosition(0, handPosition);

            OVRSemanticClassification anchor = hit.collider.gameObject.GetComponent<OVRSemanticClassification>();
            

            if (anchor != null)
            {
                debugText.text = "Hit an anchor with the Label: " + string.Join(", ", anchor.Labels);
                Vector3 endPoint = anchor.transform.position;
                lineRenderer.SetPosition(1, endPoint);
            }
            else
            {
                lineRenderer.SetPosition(1, hit.point);
            }
        }

        
    }
}
