using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DropdownManager : MonoBehaviour
{
    public int selectedMenu = 0;
    public GameObject[] gridContainers;

    public void ChangeCategory()
    {
        /*
        gridContainers[selectedMenu].gameObject.SetActive(false);
        selectedMenu = (selectedMenu + 1) % gridContainers.Length;
        gridContainers[selectedMenu].gameObject.SetActive(true);
        */

        // Iterate through all grid containers

        selectedMenu = (selectedMenu + 1) % gridContainers.Length;

        for (int i = 0; i < gridContainers.Length; i++)
        {
            gridContainers[i].gameObject.SetActive(false);
            if (i == selectedMenu)
            {
                gridContainers[i].gameObject.SetActive(true);
            }
        }
    }
}
