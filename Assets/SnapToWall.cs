using Oculus.Interaction;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class SnapToWall : MonoBehaviour
{

    private Transform toSnap;
    private Transform headObject;
    private DistanceGrabInteractable _interactable;
    [SerializeField] private LineRenderer lineRenderer;
     Vector3 projectionOnNearestWall = default;
    float distanceToNearestWall;
    Vector3 wallNormal;
    Outline thisOutline;
    BoxCollider[] possibleWalls;
    Vector3 lastPosition;
    Vector3 offset;
    private Transform toSnapInteractable;
    public int rotateIncrement = 0;
    int frameCount = 0;
    

    private void Start()
    {
        toSnap = GetComponent<Transform>();
        Collider collider = GetComponent<Collider>();
        headObject = FindObjectOfType<Camera>().gameObject.transform;
        thisOutline = GetComponentInChildren<Outline>();
        possibleWalls = FindObjectOfType<GetFirstColliders>().roomWalls;
        toSnapInteractable = GetComponentInChildren<Grabbable>().transform;
    }

    private void Update()
    {
        if(thisOutline.enabled == true && (toSnap.transform.position - lastPosition).magnitude > 0.1 && frameCount > 5)
        {
                // Go through all OVRSceneManager planes and find the nearest collider and the point corresponding to it.
                if(lineRenderer.enabled == false)
                    lineRenderer.enabled = true;
                lineRenderer.SetPosition(0, toSnap.transform.position);
                distanceToNearestWall = 999999;
                foreach (BoxCollider wall in possibleWalls)
                {
                    Collider wallSurface = wall.GetComponent<Collider>();
                    Vector3 wallProjection = wallSurface.ClosestPoint(toSnap.transform.position);
                    float wallDistance = Vector3.Distance(wallProjection, toSnap.transform.position);
                    if (wallDistance < distanceToNearestWall && !wall.gameObject.Equals(toSnap.gameObject)
                        && wall.gameObject.tag != "Menu" && Mathf.Abs(wallProjection.y - toSnap.transform.position.y) < 0.1)
                    {
                        projectionOnNearestWall = wallProjection;
                        distanceToNearestWall = wallDistance;
                        wallNormal = toSnap.transform.position - projectionOnNearestWall;
                        // toSnap.transform.rotation = Quaternion.LookRotation((toSnap.transform.position - projectionOnNearestWall), Vector3.up);
                    }
                }
                lineRenderer.SetPosition(1, projectionOnNearestWall);
            frameCount = 0;
        }
        frameCount++;
    }

    public void SnapToWallPosition()
    {
        if(distanceToNearestWall > 0.1)
        {
            toSnap.rotation = Quaternion.LookRotation(wallNormal, Vector3.up) * Quaternion.Euler(0, rotateIncrement * 90, 0);
            offset = toSnap.position - toSnapInteractable.position;
            toSnap.transform.position = projectionOnNearestWall + /*(Quaternion.Euler(0, rotateIncrement*90, 0) **/ offset;// + wallNormal * (toSnap.GetComponent<BoxCollider>().size.z/2);
            lastPosition = toSnap.transform.position;
            lineRenderer.enabled = false;
            distanceToNearestWall = 0;
        }
    }
}