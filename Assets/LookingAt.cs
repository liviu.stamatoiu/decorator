using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookingAt : MonoBehaviour
{
    public Transform centerEyeAnchor; // Assign the CenterEyeAnchor in the inspector
    public GameObject targetObject; // Which object?
    public GameObject targetObject2; // Which object?
    public GameObject toggleableObject; // What do we turn on when targetObject is hit?
    public GameObject toggleableObject2; // What do we turn on when targetObject is hit?
    public float raycastDistance = 100.0f; // Define the distance for the raycast
    private bool objectOn = false;
    //[SerializeField] private LineRenderer lineRenderer;
    private LayerMask uiMenuLayer;

    private void Start()
    {
        uiMenuLayer = LayerMask.GetMask("MenuLayer");
    }

    void Update()
    {
        // Perform the raycast
        Ray ray = new Ray(centerEyeAnchor.position, centerEyeAnchor.forward);
        RaycastHit hit;
        //lineRenderer.SetPosition(0, ray.origin);
        if (Physics.Raycast(ray, out hit, raycastDistance, uiMenuLayer))
        {
            //lineRenderer.SetPosition(1, hit.point);
            if (hit.collider.gameObject == targetObject || hit.collider.gameObject == targetObject2)
            {
                if (objectOn == false)
                {
                    objectOn = true;
                    toggleableObject.SetActive(true);
                    toggleableObject2.SetActive(false);
                }
            }
            else
            {
                if (objectOn == true)
                {
                    objectOn = false;
                    toggleableObject.SetActive(false);
                    toggleableObject2.SetActive(true);
                }
            }
        }
        else
        {
            objectOn = false;
            toggleableObject.SetActive(false);
            toggleableObject2.SetActive(true);
            //lineRenderer.SetPosition(1, ray.origin + ray.direction * raycastDistance);
        }
            
    }

}
