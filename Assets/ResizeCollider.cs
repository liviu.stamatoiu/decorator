using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class ResizeCollider : MonoBehaviour
{

    public float scaleFactor = 1;
    void OnDrawGizmos()
    {

        BoxCollider col = GetComponent<BoxCollider>();

        // Thanks thouala-wellfiredLtd on https://forum.unity.com/threads/calculating-a-bound-of-a-grouped-model.101121/

        Bounds bounds = new Bounds(this.transform.position, Vector3.zero);
        foreach (MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>())
        {
            bounds.Encapsulate(renderer.bounds);
        }

        // Adjust the box collider based on the combined bounds
        col.center = bounds.center;
        col.size = Vector3.Scale(bounds.size, new Vector3((float)scaleFactor, (float)scaleFactor, (float)scaleFactor));
    }
}
