using Meta.WitAi;
using TMPro;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    public GameObject selectedObject;
    public TextMeshProUGUI debugText;
    private Outline objOutline;
    public ParticleSystem deletionParticles;
    public AudioSource deletionSound;
    private bool disableState = false;
    private bool previousDisableState = false;
    public GameObject[] gameObjectsToToggle;
    public AudioSource musicToPause;
    public AudioSource buttonOff;
    public AudioSource buttonOn;

    public void SelectObject(GameObject obj)
    {

        // Deselect the previously selected object
        DeselectObject();

        // Select the new object
        selectedObject = obj;

        if(objOutline = selectedObject.GetComponentInChildren<Outline>())
        {
            objOutline.enabled = true;
        }

    }

    public void DeselectObject()
    {
        if(objOutline is not null)
            objOutline.enabled = false;

        selectedObject = null;
        objOutline = null;
    }

    public void DestroySelectedObject()
    {
        if (selectedObject != null)
        {
            Vector3 objPos = selectedObject.transform.position;
            Instantiate(deletionParticles, objPos, Quaternion.identity);
            deletionSound.Play();
            Destroy(selectedObject);
            selectedObject = null;
            objOutline = null;
        }
    }

    private void Update()
    {

        if (OVRInput.GetDown(OVRInput.Button.Start, OVRInput.Controller.Hands))
            disableState = true;
        else
            disableState = false;

        if(previousDisableState == false && disableState == true)
        {
            DeselectObject();
            toggleObjectsState();
            if (musicToPause.isPlaying)
            {
                musicToPause.Pause();
                buttonOff.Play();
            }
            else
            {
                musicToPause.Play();
                buttonOn.Play();
            }
        }

        previousDisableState = disableState;
    }
    public void toggleObjectsState()
    {
        foreach (GameObject go in gameObjectsToToggle)
        {
            go.SetActive(!go.activeSelf);
        }
    }

}
